package com.vishal.rest.controller;

import java.net.URI;
import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.mvc.WebMvcLinkBuilder;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import com.vishal.rest.dao.UserDao;
import com.vishal.rest.exception.UserNotFoundException;
import com.vishal.rest.model.User;

import ch.qos.logback.classic.pattern.MethodOfCallerConverter;
import net.bytebuddy.asm.Advice.This;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.*;


@RestController
public class UserResources {
	@Autowired
	private UserDao userDao;
	
	@GetMapping("/users")
	public List<User> getAllUsers(){
		return userDao.getAllUser();
	}
	
	@GetMapping("/user/{id}")
	public EntityModel<User> getUser(@PathVariable int id) {
		User user = userDao.findOne(id);
		if(user == null)
			throw new UserNotFoundException("id-" + id);	
		
		EntityModel<User> model = new EntityModel<User>(user);
		
		WebMvcLinkBuilder linkTo = linkTo(methodOn(this.getClass()).getAllUsers());
		model.add(linkTo.withRel("all-users"));		
		return model;
	}
	
	@PostMapping("/users")
	public ResponseEntity<Object> saveUser(@Valid @RequestBody User user) {
		User savedUser = userDao.saveUser(user);
		URI location = ServletUriComponentsBuilder
				.fromCurrentRequest()
				.path("/{id}")
				.buildAndExpand(savedUser.getId()).toUri();
		//to return proper response code like here 201 which means resource created
		return ResponseEntity.created(location).build();
	}
	
	@DeleteMapping("/user/{id}")
	public User deleteUSer(@PathVariable int id) {
		User user = userDao.deleteOne(id);
		if(user == null)
			throw new UserNotFoundException("User not found with id:" + id);
		
		return user;
	}
}
