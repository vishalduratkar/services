package com.vishal.rest.dao;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.stereotype.Component;

import com.vishal.rest.model.User;

@Component
public class UserDao {
	private static List<User> users = new ArrayList<>();
	private static int count = 3;
	static {
		users.add(new User(1, "Adam", new Date()));
		users.add(new User(2, "Eve", new Date()));
		users.add(new User(3, "Jack", new Date()));
	}
	
	public List<User> getAllUser() {
		return users;
	}
	
	public User saveUser(User user) {
		if(user.getId() == null)
			user.setId(++count);
		users.add(user);
		return user;
	}
	 
	public User findOne(Integer id) {
		for(User user : users)
			if(user.getId() == id)
				return user;
			return null;
	}
	
	public User deleteOne(Integer id) {
		for (User user : users) {
			if(user.getId()== id) {
				return user;
			}
		}
		return null;
	}
}
