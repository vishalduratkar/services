package com.vishal.rest.exception;

import java.util.Date;

//Customized response object in case of exception
public class ExceptionResponse {
	private Date dateTimeStamp;
	private String message;
	private String details;
	
	public ExceptionResponse(Date dateTimeStamp, String message, String details) {
		super();
		this.dateTimeStamp = dateTimeStamp;
		this.message = message;
		this.details = details;
	}
	public Date getDateTimeStamp() {
		return dateTimeStamp;
	}
	public void setDateTimeStamp(Date dateTimeStamp) {
		this.dateTimeStamp = dateTimeStamp;
	}
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public String getDetails() {
		return details;
	}
	public void setDetails(String details) {
		this.details = details;
	}
	
}
