package com.vishal.ms.ratingsdateservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;

@SpringBootApplication
@EnableEurekaClient   //not required
public class RatingsDateServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(RatingsDateServiceApplication.class, args);
	}

}
