package com.vishal.ms.moviecatalogservice.controller;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import com.vishal.ms.moviecatalogservice.model.CatalogItem;
import com.vishal.ms.moviecatalogservice.model.Movie;
import com.vishal.ms.moviecatalogservice.model.Rating;
import com.vishal.ms.moviecatalogservice.model.UserRating;

@RestController
@RequestMapping("/catalog")
public class MovieCatlogResource {
	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	private WebClient.Builder webClientBuilder;

	@RequestMapping("/{userid}")
	public List<CatalogItem> getCatalog(@PathVariable("userid") String userId) {
		UserRating userRating = restTemplate.getForObject("http://RATING-DATE-SERVICE/ratingsdata/user/" + userId, UserRating.class);
		return userRating.getUserRating().stream().map(rating -> {
			 Movie movie = restTemplate.getForObject("http://MOVIE-INFO-SERVICE/movie/" +
			 rating.getMovieId(), Movie.class);
			
			return new CatalogItem(movie.getName(), "TestMovie", rating.getRating());
		}).collect(Collectors.toList());
		// return Collections.singletonList(new CatalogItem("Transformers", "test", 4));
		
		/* 
		 UserRating userRating = restTemplate.getForObject("http://localhost:8083/ratingsdata/user/" + userId, UserRating.class);
		 								OR
		 * Movie movie = webClientBuilder.build().get().uri("http://localhost:8082/movie/\" + rating.getMovieId()")
		.retrieve().bodyToMono(Movie.class).block(); block forcefully and wait until we get responce so in short we are making Async call to Sync call
		In fact webClientBuilder is used to make async call
		*/ 
	}
}
