ContentNegotiation for XML just added dependency
<groupId>com.fasterxml.jackson.dataformat</groupId>
<artifactId>jackson-dataformat-xml</artifactId>

POST http://localhost:8080/users
Content-Type application/xml  in header

GET http://localhost:8080/users
Accept	application/xml
---------------------------------------
Swagger Springfox Swagger2
JSON API documentation for spring based applications 
---------------------------------------
Monitoring APIs with Spring Boot Actuator
http://localhost:8080/actuator 

<dependency>
 <groupId>org.springframework.boot</groupId>
 <artifactId>spring-boot-starter-actuator</artifactId>
</dependency>

<dependency>
    <groupId>org.springframework.data</groupId>
    <artifactId>spring-data-rest-hal-browser</artifactId>
</dependency>
http://localhost:8080/actuator
management.endpoints.web.exposure.include=*

http://localhost:8080/browser/index.html#/
----------------------------------------------

Filtering
	Static
GET http://localhost:8080/filtering
	@JsonIgnore  Skip some items  over field or   recommended
	@JsonIgnoreProperties(value={"field1","field2"}) over bean
GET http://localhost:8080/filteringList

	dynamic
@JsonFilter("someBeanFilter") over bean
@GetMapping("/filtering")
	public MappingJacksonValue retrieveSomeBean() {
		SomeBean someBean = new SomeBean("value1","value2","value3");
		SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept("fiedld1","fiedld2");
		FilterProvider filterProvider = new SimpleFilterProvider().addFilter("someBeanFilter", filter );
		MappingJacksonValue mapping = new MappingJacksonValue(someBean);
		mapping.setFilters(filterProvider);
		return mapping;
	}
--------------------------------------------------
Versioning
http://localhost:8080/v1/person
http://localhost:8080/v2/person

http://localhost:8080/person/param?version=1
http://localhost:8080/person/param?version=2

http://localhost:8080/person/header
X-API-VERSION 1   in header

http://localhost:8080/person/produces
Accept application/vnd.company.app-v2+json  in header