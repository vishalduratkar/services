package com.vishal.rest.controller;

import java.util.Arrays;
import java.util.List;

import org.springframework.http.converter.json.MappingJacksonValue;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ser.FilterProvider;
import com.fasterxml.jackson.databind.ser.impl.SimpleBeanPropertyFilter;
import com.fasterxml.jackson.databind.ser.impl.SimpleFilterProvider;
import com.vishal.rest.model.SomeBean;

@RestController
public class FilteringController {

	/* Static filtering 
	@GetMapping("/filtering")
	public SomeBean retrieveSomeBean() {
		return new SomeBean("value1","value2","value3");
	}
	
	@GetMapping("/filteringList")
	public List<SomeBean> retrieveSomeBeanList() {
		return Arrays.asList(new SomeBean("value1","value2","value3"),new SomeBean("value1","value2","value3"),new SomeBean("value1","value2","value3"));
	} */
	
	/* Dynamic filtering */
	@GetMapping("/filtering")
	public MappingJacksonValue retrieveSomeBean() {
		SomeBean someBean = new SomeBean("value1","value2","value3");
		SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept("fiedld1","fiedld2");
		FilterProvider filterProvider = new SimpleFilterProvider().addFilter("someBeanFilter", filter );
		MappingJacksonValue mapping = new MappingJacksonValue(someBean);
		mapping.setFilters(filterProvider);
		return mapping;
	}
	
	@GetMapping("/filteringList")
	public MappingJacksonValue retrieveSomeBeanList() {
		List<SomeBean> list = Arrays.asList(new SomeBean("value1","value2","value3"),new SomeBean("value1","value2","value3"),new SomeBean("value1","value2","value3"));
		SimpleBeanPropertyFilter filter = SimpleBeanPropertyFilter.filterOutAllExcept("fiedld2","fiedld3");
		FilterProvider filterProvider = new SimpleFilterProvider().addFilter("someBeanFilter", filter );
		MappingJacksonValue mapping = new MappingJacksonValue(list);
		mapping.setFilters(filterProvider);
		return mapping;
	}
}
