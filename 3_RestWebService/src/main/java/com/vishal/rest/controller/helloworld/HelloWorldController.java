package com.vishal.rest.controller.helloworld;

import java.util.Locale;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RestController;

import com.vishal.rest.model.HelloWorlBean;

@RestController
public class HelloWorldController {
	
	@Autowired
	private MessageSource messageSource;
	
	@GetMapping("/hello")
	public String returnHelloWorld() {
		return "Hello Vishal Duratkar";
	}
	
	@GetMapping("/hello-world-bean")
	public HelloWorlBean helloWorldBean() {
		return new HelloWorlBean("Hello Vishal from Bean");
	}
	
	@GetMapping("/hello-world-bean/path/{name}")
	public HelloWorlBean helloWorldBeanPath(@PathVariable String name) {
		return new HelloWorlBean(String.format("Hello Vishal from path, %s",name));
	}
	
	/*
	@GetMapping("/helloworldI18")
	public String helloWorldInterniationalized(@RequestHeader(name="Accept-Language", required = false) Locale locale) {
		return messageSource.getMessage("good.morning.message", null, locale);
	}
	
				OR  because above one is painful  because it need to be applied on evry method need to be internationalized
				so spring provide alternative approach
	*/
	
	@GetMapping("/helloworldI18")
	public String helloWorldInterniationalized() {
		return messageSource.getMessage("good.morning.message", null, LocaleContextHolder.getLocale());
	}
}
