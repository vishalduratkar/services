package com.vishal.rest.model;

import com.fasterxml.jackson.annotation.JsonFilter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

//@JsonIgnoreProperties(value={"fiedld1","fiedld2"}) Static filtering 
@JsonFilter("someBeanFilter")
public class SomeBean {

	private String fiedld1;
	private String fiedld2;
	
	//@JsonIgnore Static filtering 
	private String fiedld3;
	
	
	public SomeBean(String fiedld1, String fiedld2, String fiedld3) {
		super();
		this.fiedld1 = fiedld1;
		this.fiedld2 = fiedld2;
		this.fiedld3 = fiedld3;
	}
	public String getFiedld1() {
		return fiedld1;
	}
	public void setFiedld1(String fiedld1) {
		this.fiedld1 = fiedld1;
	}
	public String getFiedld2() {
		return fiedld2;
	}
	public void setFiedld2(String fiedld2) {
		this.fiedld2 = fiedld2;
	}
	public String getFiedld3() {
		return fiedld3;
	}
	public void setFiedld3(String fiedld3) {
		this.fiedld3 = fiedld3;
	}
	
}
