package com.vishal.ms.ratingsdateservice.controller;


import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.vishal.ms.ratingsdateservice.model.Rating;
import com.vishal.ms.ratingsdateservice.model.UserRating;

@RestController
@RequestMapping("/ratingsdata")
public class RatingsResource {

    @RequestMapping("/movies/{movieId}")
    public Rating getMovieRating(@PathVariable("movieId") String movieId) {
        return new Rating(movieId, 4);
    }

    
    @RequestMapping("/user/{userId}")
    public UserRating getUserRatings(@PathVariable("userId") String userId) {
    	List<Rating> ratings = Arrays.asList(new Rating("500", 4), new Rating("501", 3));
    	/*
        UserRating userRating = new UserRating();
        userRating.setUserRating(ratings);*/
        return new UserRating(ratings);
    } 
}