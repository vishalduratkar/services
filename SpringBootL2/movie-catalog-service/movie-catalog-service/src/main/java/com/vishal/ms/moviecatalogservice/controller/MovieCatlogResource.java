package com.vishal.ms.moviecatalogservice.controller;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.vishal.ms.moviecatalogservice.model.CatalogItem;
import com.vishal.ms.moviecatalogservice.model.Movie;
import com.vishal.ms.moviecatalogservice.model.Rating;
import com.vishal.ms.moviecatalogservice.model.UserRating;
import com.vishal.ms.moviecatalogservice.service.MovieInfoService;
import com.vishal.ms.moviecatalogservice.service.RatingInfoService;

@RestController
@RequestMapping("/catalog")
public class MovieCatlogResource {
	
	@Autowired
	private MovieInfoService movieCatlogResource;
	
	@Autowired
	private RatingInfoService ratingInfoService;
	
	@HystrixCommand
	@GetMapping("/{userid}")
	public List<CatalogItem> getCatalog(@PathVariable("userid") String userId) {
		UserRating userRating = ratingInfoService.getUserRating(userId);
		return userRating.getUserRating().stream().map(rating -> movieCatlogResource.getCatalogItem(rating)).collect(Collectors.toList());
	}
}


/*
 * @RequestMapping("/abs/{userid}")
	@HystrixCommand(fallbackMethod = "getFallbackCatalog")
 public List<CatalogItem> getCatalog(@PathVariable("userid") String userId) {
		UserRating userRating = restTemplate.getForObject("http://RATING-DATE-SERVICE/ratingsdata/user/" + userId, UserRating.class);
		return userRating.getUserRating().stream().map(rating -> {
			 Movie movie = restTemplate.getForObject("http://MOVIE-INFO-SERVICE/movie/" +
			 rating.getMovieId(), Movie.class);
			
			return new CatalogItem(movie.getName(), movie.getDescription(), rating.getRating());
		}).collect(Collectors.toList());
		// return Collections.singletonList(new CatalogItem("Transformers", "test", 4));
		
		
	}
	
	public List<CatalogItem> getFallbackCatalog(@PathVariable("userid") String userId) {
		return Arrays.asList(new CatalogItem("No movie", "",  0));
		
	}
 
 */

/* 
 
 	@Autowired
	private WebClient.Builder webClientBuilder;
	
UserRating userRating = restTemplate.getForObject("http://localhost:8083/ratingsdata/user/" + userId, UserRating.class);
								OR
* Movie movie = webClientBuilder.build().get().uri("http://localhost:8082/movie/\" + rating.getMovieId()")
.retrieve().bodyToMono(Movie.class).block(); block forcefully and wait until we get responce so in short we are making Async call to Sync call
In fact webClientBuilder is used to make async call
*/ 
