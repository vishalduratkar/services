package com.vishal.ms.moviecatalogservice.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.vishal.ms.moviecatalogservice.model.CatalogItem;
import com.vishal.ms.moviecatalogservice.model.Movie;
import com.vishal.ms.moviecatalogservice.model.Rating;

@Service
public class MovieInfoService {
	@Autowired
	private RestTemplate restTemplate;
	
	@HystrixCommand(fallbackMethod = "getFallbackCatalogItem")
	public CatalogItem getCatalogItem(Rating rating) {
		return getCatalogItem1(rating);
	}
	
	public CatalogItem getFallbackCatalogItem(Rating rating) {
		return new CatalogItem("No movie", "No Desc",  rating.getRating());
		
	}
	public CatalogItem getCatalogItem1(Rating rating) {
		Movie movie = restTemplate.getForObject("http://MOVIE-INFO-SERVICE/movie/" +
				 rating.getMovieId(), Movie.class);
		return new CatalogItem(movie.getName(), movie.getDescription(), rating.getRating());
	}
	
}
